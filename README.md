#README#

##Project Overview##
Designed and implemented a Dynamic Memory Allocator, which implements functions like malloc, free, realloc and calloc. The strategy to manage memory is mainly based on segregated free list, first fit placement and boundary tag coalescing.