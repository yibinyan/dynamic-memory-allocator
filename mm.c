/*
 * mm.c
 * hbovik - Harry Bovik
 *
 * Name: Yibin Yan Andrew ID: yibiny
 * 
 * Use Segregated list (each list contains different-sized block whose size are members of the sizeclass) to organize free blocks
 * Each Header of block contains a alloc bit and a previous-free bit  
 * Each block in the free lists contains a succ pointer that point to the free block(in same size class) that next to it 
 * Search Algorithm: First fit
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include "contracts.h"

#include "mm.h"
#include "memlib.h"


// Create aliases for driver tests
// DO NOT CHANGE THE FOLLOWING!
#ifdef DRIVER
#define malloc mm_malloc
#define free mm_free
#define realloc mm_realloc
#define calloc mm_calloc
#endif

/*
 *  Logging Functions
 *  -----------------
 *  - dbg_printf acts like printf, but will not be run in a release build.
 *  - checkheap acts like mm_checkheap, but prints the line it failed on and
 *    exits if it fails.
 */

#ifndef NDEBUG
#define dbg_printf(...) printf(__VA_ARGS__)
#define checkheap(verbose) do {if (mm_checkheap(verbose)) {  \
                             printf("Checkheap failed on line %d\n", __LINE__);\
                             exit(-1);  \
                        }}while(0)
#else
#define dbg_printf(...)
#define checkheap(...)
#endif

#define WSIZE 4
#define DSIZE 8
#define CHUNKSIZE (1 << 9)

char *heap_listp;
size_t *free_listp;

static void *coalesce(void *bp);
static void *find_fit(size_t asize);
static void place(void *bp, size_t asize);
static void *extend_heap(size_t words, int initial);

/*
 *  Helper functions
 *  ----------------
 */

// Return whether the pointer is in the heap.
static inline int in_heap(const void* p) {
	    return p <= mem_heap_hi() && p >= mem_heap_lo();
}


//Get the sizeclass which the free block belongs to
static inline unsigned int get_sizeclass(const size_t size){
	//size_t temp = 0x10; 
	unsigned int sizeclass = 0;

	if (size <= 16){
		sizeclass = 0;
	}

	else if (size <= 32){
		sizeclass = 1;
	}
	
	else if (size <= 64){
		sizeclass = 2;
	}

	else if (size <= 128){
		sizeclass = 3;
	}
	
	else if (size <= 256){
		sizeclass = 4;
	}

	else if (size <= 512){
		sizeclass = 5;
	}

	else if (size <= 1024){
		sizeclass = 6;
	}
	
	
	else if (size <= 2048){
		sizeclass = 7;
	}
	
	else if (size <= 4096){
		sizeclass = 8;
	}

	else {
		sizeclass = 9;
	}
	
	return sizeclass;
}



/* Return whether the pointer is in the heap.
static int in_heap(const void* p) {
    return p <= mem_heap_hi() && p >= mem_heap_lo();
}
*/


/*
 *  Block Functions
 *  ---------------
 *  The functions below manipulate blocks. 
 *  Description:
 * 		unsigned int get(char* p): Read word at address p
 *  	void put(char *p, unsigned int val): Write word at address p
 *		unsigned int get_size(char *p): Get the size of the given block 
 *		unsigned int get_prev_free(char *p): Get prev_free bit from address p
 *		unsigned int get_alloc(char *p): Get the allocate bit from address p
 * 		unsigned int pack(unsigned int size, unsigned int alloc): Pack a size and allocated bit into a word
 *		char* header(char *bp): Given block ptr bp, compute address of its header
 *		char* footer(char *bp): Given block ptr bp, compute address of its footer
 *		int is_prev_alloc(char *bp): Check if previous block is free
 *		char* next_blkp(char *bp):Given block ptr bp, compute address of next block
 *		char* prev_blkp(char *bp):Given block ptr bp, compute address of previous block
 *		void remove_from_list(size_t *bp): Remove block bp from free list
 *  
 */


//Read and write a word at address p
static inline unsigned int get(char* p){
    REQUIRES(p != NULL);
    REQUIRES(in_heap(p));
	
	return *((unsigned int *)(p));
} 

static inline void put(char *p, unsigned int val){
    REQUIRES(p != NULL);
    REQUIRES(in_heap(p));
	
	*((unsigned int *)(p)) = val;

	return;
}

// Return the size of the given block 
static inline unsigned int get_size(char *p) {
	REQUIRES(p != NULL);
    REQUIRES(in_heap(p));
	return (get(p) & ~0x7);
}

//Return the prev_free flag from address p
static inline unsigned int get_prev_free(char *p) {
	REQUIRES(p != NULL);
    REQUIRES(in_heap(p));

	return (get(p) & 0x2);
}


// Return the allocated fields from address p
static inline unsigned int get_alloc(char *p) {
	REQUIRES(p != NULL);
    REQUIRES(in_heap(p));

	return (get(p) & 0x1);
}

//Pack a size and allocated bit into a word
static inline unsigned int pack(unsigned int size, unsigned int alloc){
	return ((size) | (alloc));
}

//Given block ptr bp, compute address of its header and footer
static inline char* header(char *bp){
	REQUIRES(bp != NULL);
	
	return (char *)(bp) - WSIZE;
}

static inline char* footer(char *bp){
	REQUIRES(bp != NULL);
    REQUIRES(in_heap(bp));
	return (char *)bp + get_size(header(bp)) - DSIZE;
}

//Check if previous block is free
static inline int is_prev_alloc(char *bp) {
	REQUIRES(bp != NULL);
    REQUIRES(in_heap(bp));
	return !(get(header(bp)) & 0x2);
}

//Given block ptr bp, compute address of next and previous blocks
static inline char* next_blkp(char *bp) {
	REQUIRES(bp != NULL);
    REQUIRES(in_heap(bp));
	
	return (char *)(bp) + get_size((char *)(bp) - WSIZE);

}

static inline char* prev_blkp(char *bp) {
	REQUIRES(bp != NULL);
	REQUIRES(in_heap(bp));
	
	return (char *)(bp) - get_size((char *)(bp) - DSIZE);
}

//Remove block bp from free list
static inline void remove_from_list(size_t *bp){
	REQUIRES(bp != NULL);
	REQUIRES(in_heap(bp));
	size_t size = get_size(header((char *)bp));
	unsigned int sizeclass = get_sizeclass(size);
	size_t *temp;

	for (temp = free_listp + sizeclass; *temp; temp = (size_t *)(*temp)){
		if (*temp == (size_t)bp){
			//dbg_printf("Remove!");
			*temp = *bp;
			//*bp = 0x0;
			break;
		}
	}
}


/*
 *  Malloc Implementation
 *  ---------------------
 *  The following functions deal with the user-facing malloc implementation.
 */

/*
 * Initialize: return -1 on error, 0 on success.
 */
int mm_init(void) {

	/*create free lists by power of two, that is, {0-16}, {17-32}, {33-64}, {65-128}...{4087-infinite}
	 *any blocks that less than 16 bytes belongs to {0-16} class
	 *there are 10 size classes*/
	free_listp = mem_sbrk(10 * DSIZE);
	
   	for (int i = 0; i < 10; i++){
		free_listp[i] = 0x0;
	}	

	/*create initial empty heap*/
	if ((heap_listp = mem_sbrk(4 * WSIZE)) == (void *)-1){
		return -1;
	}

	put(heap_listp, 0);
	put(heap_listp + (1*WSIZE), pack(DSIZE, 1));
	put(heap_listp + (2*WSIZE), pack(DSIZE, 1));
	put(heap_listp + (3*WSIZE), pack(0, 1));
	heap_listp += (2*WSIZE);

	if (extend_heap(CHUNKSIZE, 1) == NULL)
		return -1;

	return 0;
}

/*
 * extend heap
 */
static void *extend_heap(size_t size, int initial){
	char *bp;
	//dbg_printf("\nExtend!");

	if ((long)(bp = mem_sbrk(size)) == -1){
		return NULL;
	}

	/*keep the prev free bit of epiloque block*/
	unsigned int old_prevfree_flag = get_prev_free(header(bp));
	put(header(bp), pack(size|old_prevfree_flag, 0));

	put(footer(bp), pack(size, 0));
	put(header(next_blkp(bp)), pack(0|0x2, 1));

	if (initial){
		return coalesce(bp);
	}else{
		return bp;
	}
}

/*
 * coalese
 */

static void *coalesce(void *bp){
	REQUIRES(bp != NULL);
	REQUIRES(in_heap(bp));

	size_t prev_alloc = is_prev_alloc(bp);
	size_t next_alloc = get_alloc(header(next_blkp(bp)));
	size_t size = get_size(header(bp));
	unsigned int sizeclass;

	//sizeclass = get_sizeclass(size);

	if (prev_alloc && next_alloc){
		//do nothing
	}

	else if (prev_alloc && !next_alloc){
		/*Remove next block from free list*/
		void *next_bp = next_blkp(bp);
		size_t next_size = get_size(header(next_bp));
		remove_from_list(next_bp);
		size += next_size;
		put(header(bp), pack(size, 0));
		put(footer(bp), pack(size, 0));
	}

	else if (!prev_alloc && next_alloc){
		/*Remove previous block from free list*/
		void *prev_bp = prev_blkp(bp);
		size_t prev_size = get_size(header(prev_bp));
		remove_from_list(prev_bp);

		size += prev_size;
		put(footer(bp), pack(size, 0));
		put(header(prev_bp), pack(size, 0));
		bp = prev_bp;
	}

	else {
		/*Remove next block from free list*/
		void *next_bp = next_blkp(bp);
		size_t next_size = get_size(header(next_bp));
		remove_from_list(next_bp);

		/*Remove previous block from free list*/
		void *prev_bp = prev_blkp(bp);
		size_t prev_size = get_size(header(prev_bp));
		remove_from_list(prev_bp);

		size += (prev_size + next_size);
		put(header(prev_bp), pack(size, 0));
		put(footer(next_bp), pack(size, 0));
		bp = prev_bp;
	}

	/*set the prev_free flag of the next block to 1 */
	void *next_bp = next_blkp(bp);
	//if (get_size(header(next_bp)) != 0){
		unsigned int old_header = get(header(next_bp));
		put(header(next_bp), old_header|0x2);
	//}
	
	sizeclass = get_sizeclass(size);
	/*link the new block to appropriate free list*/

	// insert in the first place
	size_t temp;
	temp = free_listp[sizeclass];
	free_listp[sizeclass] = (size_t)bp;
	*((size_t *)bp) = temp;	
	
	//dbg_printf("coalesce!");
	return bp;
}

static void *find_fit(size_t asize){
	size_t *bp;
	size_t *temp;
	unsigned int sizeclass;

	//dbg_printf("find fit!");

	/*do a fist fit search of the appropriate free list for a block that fits,
	 	if can not find then search the free list for the next larger size class*/
	for (sizeclass = get_sizeclass(asize); sizeclass < 10; sizeclass++){
		temp = free_listp + sizeclass;
		for (bp = (size_t *)(free_listp[sizeclass]); 
					bp != (size_t *)0x0; bp = (size_t *)(*(bp))){

			if (asize <= get_size(header((char *)bp))){
				/*remove block from list*/
				*temp = *bp;
				return bp;
			}

			temp = bp;
		}
	}

	return NULL;
}

static void place(void *bp, size_t asize){
	size_t csize = get_size(header(bp));
	void *next_bp;
	void *next_next_bp;

	//dbg_printf(" Place= %ld", asize);
	/*if the remainder size greater than 16 bytes, then split */
	if ((csize - asize) >= (2 * DSIZE)){
		/*keep the old prev_free flag and set to new header*/
		unsigned int old_prevfree_flag = get_prev_free(header(bp));
		put(header(bp), pack(asize|old_prevfree_flag, 1));
		
		next_bp = next_blkp(bp);
		put(header(next_bp), pack(csize-asize, 0));
		put(footer(next_bp), pack(csize-asize, 0));
		
		/*set the prev_free flag of the block after remainder to 1 */
		next_next_bp = next_blkp(next_bp);
		//if (get_size(header(next_next_bp)) != 0){
			unsigned int old_header = get(header(next_next_bp));
			put(header(next_next_bp), old_header|0x2);
		//}
		
		coalesce(next_bp);
	}
	else {
		/*keep the old prev_free flag and set to new header*/
		unsigned int old_prevfree_flag = get_prev_free(header(bp));
		put(header(bp), pack(csize|old_prevfree_flag, 1));
	
		next_bp = next_blkp(bp);
		//if (get_size(header(next_bp)) != 0){
			unsigned int old_header = get(header(next_bp));
			old_header = old_header & (~0x02);
			put(header(next_bp), old_header);
		//}
	}
}

/*
 * malloc
 */
void *malloc (size_t size) {

	checkheap(1);  // Let's make sure the heap is ok!

	size_t asize;
	size_t extendsize;
	char *bp;


	if (size <= 0)
		return NULL;

	//dbg_printf("\nmalloc:%ld ", size);
	/*Adjust block size to include overhead and alignment requirement*/
	if (size < DSIZE)
		asize = 16;
	else
		/*0xb = WSIZE + (DSIZE - 1)*/
		asize = ((size + 0xb) / DSIZE) * DSIZE;

	if ((bp = find_fit(asize)) != NULL){
		place(bp, asize);
		//dbg_printf(" success! ");
		return bp;
	}

	/*No fit found. Get more memory and place the block*/
	if (asize >= CHUNKSIZE){
		extendsize = asize;
	}else{
		extendsize = CHUNKSIZE;
	}

	if ((bp = extend_heap(extendsize, 0)) == NULL)
		return NULL;

	remove_from_list((size_t *)bp);
	place(bp, asize);

	return bp;
}

/*
 * free
 */
void free (void *ptr) {
	//REQUIRES(in_heap(ptr));
	//dbg_printf("Free!");
	//dbg_printf(" FreeAddress=%p", ptr);
	if (ptr > (void *)0x0){
		unsigned int old_prevfree_flag = get_prev_free(header(ptr));
		size_t size = get_size(header(ptr));

		put(header(ptr), pack(size|old_prevfree_flag, 0));
		put(footer(ptr), pack(size|old_prevfree_flag, 0));

		coalesce(ptr);
	}
}

/*
 * realloc - you may want to look at mm-naive.c
 */
void *realloc(void *oldptr, size_t size) {

	//dbg_printf("realloc ");
	size_t oldsize;
	void *newptr;

  	/* If size == 0 then this is just free, and we return NULL. */
	if(size == 0) {
		free(oldptr);
		return 0;
  	}

  	/* If oldptr is NULL, then this is just malloc. */
  	if(oldptr == NULL) {
    	return malloc(size);
  	}

  	newptr = malloc(size);

 	/* If realloc() fails the original block is left untouched  */
	if(!newptr) {
  	  	return 0;
  	}

  	/* Copy the old data. */
  	oldsize = get_size(header(oldptr)) - WSIZE;
  	if(size < oldsize) 
		oldsize = size;
  	memcpy(newptr, oldptr, oldsize);

  	/* Free the old block. */
  	free(oldptr);

  	return newptr;
}

/*
 * calloc - you may want to look at mm-naive.c
 */
void *calloc (size_t nmemb, size_t size) {
	//dbg_printf("Calloc ");
	size_t bytes = nmemb * size;
	void *newptr;

	newptr = malloc(bytes);
	memset(newptr, 0, bytes);

  	return newptr;
}

// Returns 0 if no errors were found, otherwise returns the error
int mm_checkheap(int verbose) {
	verbose = verbose;
	void *bp;

	/*Check epilogue*/
	if (get_size((char *)mem_heap_hi() - 3) != 0x0){
		printf("ERROR: Epilogue error\n");
		return 1;
	}

	/*Check prologue*/
	if ((get(header(heap_listp)) != (DSIZE|1)) || (get(footer(heap_listp)) != (DSIZE|1))){
		printf("ERROR: Prologue error\n");
		return 1;
	}

	/*Check heap boundaries*/
	if ((size_t)(mem_heap_lo()) < 0x800000000 || (size_t)(mem_heap_hi()) >= 0x900000000){
		printf("ERROR: heap boundaries error\n");
		return 1;
	}

	/*Check each block's address alignment, header, footer, etc.*/
	bp = next_blkp(heap_listp);
	unsigned int prev_free = 0;
	while (get_size(header(bp)) != (0x0)){
		size_t size = get_size(header(bp));

		if ((size_t)(bp) % 8 != 0){
			printf("ERROR: Block address %p alignment error\n", bp);
			return 1;
		}
		
		/*Check Size*/
		if (size % 8 != 0){
			printf("ERROR: Block %p Size alignment error\n", bp);
			return 1;
		}

		if (size < 16){
			printf("ERROR: Block %p is too small\n", bp);
			return 1;
		}	

		/*Check previous free bit consistency*/
		if (prev_free != (!is_prev_alloc(bp))){	
			printf("ERROR: Block %p previous free bit consistency error\n", bp);
			return 1;
		}

		/*Make sure no two consecutive free blocks in the heap*/
		if (prev_free && !get_alloc(header(bp))){
			printf("ERROR: Block %p two consecutive free blocks in the heap\n", bp);
			return 1;
		}
		
		prev_free = !get_alloc(header(bp));
		
		bp = next_blkp(bp);
	}

	/*Check Freelist*/
	size_t *bp1;
	size_t *temp;
	unsigned int sizeclass;

	for (sizeclass = 0; sizeclass < 10; sizeclass++){
		temp = free_listp + sizeclass;
		for (bp1 = (size_t *)(free_listp[sizeclass]); 
					bp1 != (size_t *)0x0; bp1 = (size_t *)(*(bp1))){
			
			/*Check next pointer consistency*/
			if (*temp != (size_t)bp1){
				printf("ERROR: next pointer of block %p is not consistent\n", (void *)temp);
				return 1;
			}
			
			/*Make sure all free list pointers between mem_heap_lo() and mem_heap_hi()*/
			if ((size_t)(bp1) < (size_t)mem_heap_lo() || (size_t)(bp1) >= (size_t)mem_heap_hi()){
				printf("ERROR: pointer is out of boundaries\n");
				return 1;
			}
			
			/*Make sure all block in each list bucket fall within bucket size range*/
			if (get_sizeclass(get_size(header((char *)bp1))) != sizeclass){
				printf("ERROR: block %p does not fall within appropriate list\n", (void *)bp1);
				return 1;
			}

			temp = bp1;
		}
	}
	
	return 0;
}

